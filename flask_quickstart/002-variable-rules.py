from flask import Flask
from markupsafe import escape

app = Flask(__name__)


@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile for that user
    return 'User %s' % escape(username)


@app.route('/post/<int:post_id>')
def show_post(post_id):
    # show the post of given id interger
    return 'Post %d' % post_id


@app.route('/path/<path:subpath>')
def show_subpath(subpath):
    # show the subpath after /
    return 'Subpath %s' % escape(subpath)


'''
You can use a converter to specify the type of the
argument like <converter:variable_name>

Converter types:

| string | (default) accepts any text without a slash|
| int | accepts positive integers|
| float | accepts positive floating point values|
| path | like string but also accepts slashes|
| uuid | accepts UUID strings|
'''
