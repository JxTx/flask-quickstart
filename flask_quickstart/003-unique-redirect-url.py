from flask import Flask
# from markupsafe import escape

app = Flask(__name__)


@app.route('/projects/')
def projects():
    return 'The project page'


@app.route('/about')
def about():
    return 'The about page'
'''
% curl http://127.0.0.1:5000/about/               
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again.</p>

% curl http://127.0.0.1:5000/about 
The about page

% curl http://127.0.0.1:5000/projects
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>Redirecting...</title>
<h1>Redirecting...</h1>
<p>You should be redirected automatically to target URL: <a href="http://127.0.0.1:5000/projects/">http://127.0.0.1:5000/projects/</a>.  If not click the link.%         
'''

