from flask import Flask, request


app = Flask(__name__)


# added so no syntax errors
def do_the_login():
    pass


# added so no syntax errors
def show_the_login():
    pass


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return do_the_login()
    else:
        return show_the_login()
