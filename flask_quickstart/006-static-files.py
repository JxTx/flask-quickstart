from flask import Flask, url_for


app = Flask(__name__)


# added for no syntax errors
@app.route('static')
def static():
    return 'static'


with app.test_request_context():
    print(url_for('static', filename='style.css'))
    # The file has to be stored on the filesystem as static/style.css


'''
Just create a folder called "static" in your package
or next to your module and it will be available at "/static" on the app.
'''
