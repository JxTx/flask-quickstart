from flask import Flask, request


app = Flask(__name__)


# added so no syntax errors
@app.route('/hello')
def hello():
    return 'hello'


with app.test_request_context('/hello', method='POST'):
    # Now do something with the request until the
    # end of the "with block"
    assert request.path == '/hello'
    assert request.method == 'POST'
