from flask import Flask, request, render_template


app = Flask(__name__)


def valid_login():
    pass


def log_the_user_in():
    pass


@app.route('/login', methods=['POST', 'GET'])
def login():
    error = None
    if request.method == 'POST':
        if valid_login(request.form['username'],
                       request.form['password']):
            return log_the_user_in(request.form['username'])
        else:
            error = 'Invalid username/password'
    return render_template('009-login.html', error=error)


'''
To access parameters submitted in the URL (?key=value)
you can use the args attribute:
'''

searchword = request.args.get('key', '')
