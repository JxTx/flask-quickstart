from flask import Flask, request
from werkzeug.utils import secure_filename

app = Flask(__name__)


@app.route('/upload', methods=['GET', 'POST'])
def upload_file_insecure():
    if request.method == 'POST':
        f = request.files['the_file']
        f.save('/var/www/uploaded_file.txt')


@app.route('/upload', methods=['GET', 'POST'])
def upload_file_secure():
    if request.method == 'POST':
        f = request.files['the_file']
        f.save('/var/www/uploads' + secure_filename(f.filename))
