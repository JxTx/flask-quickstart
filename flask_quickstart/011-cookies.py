from flask import Flask, request, make_response, render_template


app = Flask(__name__)


# reading cookies
@app.route('/')
def index():
    username = request.cookies.get('username')
    print(username)
    return 'check console'


# storing cookies
@app.route('/')
def index2():
    resp = make_response(render_template('...'))
    resp.set_cookie('username', 'the username')
    return resp
