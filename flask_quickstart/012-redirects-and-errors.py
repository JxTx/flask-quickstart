from flask import Flask, abort, redirect, url_for, render_template


app = Flask(__name__)


def this_never_executed():
    pass


@app.route('/')
def index():
    return redirect(url_for('login'))


@app.route('/login')
def login():
    abort(401)
    this_never_executed()


@app.errorhandler(404)
def page_not_found(error):
    return render_template('012-page_not_found.html'), 404
