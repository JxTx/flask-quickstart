from flask import Flask, url_for, jsonify


app = Flask(__name__)


# added for no syntax errors
def get_current_user():
    pass


# added for no syntax errors
def get_all_users():
    pass


# returning a "dict" will be converted to a JSON response
@app.route('/me')
def me_api():
    user = get_current_user()
    return {
        'username': user.username,
        'theme': user.theme,
        'image': url_for('user_image', filename=user.image)
    }


# JSON responses for types other than "dict"
@app.route('users')
def users_api():
    users = get_all_users()
    return jsonify([user.to_json() for user in users])
